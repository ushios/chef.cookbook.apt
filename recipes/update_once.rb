#
# Cookbook Name:: apt
# Recipe:: update_once
#
# Copyright 2013, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

dir = '/var/chef/apt'

directory dir do
	recursive true
	action :create
	owner 'root'
	group 'root'
	mode 0744
end

execute 'apt update once' do
	command "apt-get update && touch #{dir}/.apt-get-update-once"
	creates "#{dir}/.apt-get-update-once"
	user 'root'
	action :run
end
